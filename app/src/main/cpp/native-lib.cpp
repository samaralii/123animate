#include <jni.h>
#include <string>

extern "C"
jstring
Java_com_ss_lumo_1booth_MainAcativity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

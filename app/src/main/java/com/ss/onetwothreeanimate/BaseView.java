package com.ss.onetwothreeanimate;

/**
 * Created by Sammie on 3/2/2017.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}

package com.ss.onetwothreeanimate;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.UUID;

/**
 * Created by Sammie on 2/2/2017.
 */

public class Utilz {


    public static class AppConstant {

        public static final String BITMAP_FILENAME = "cachebitmaps";
        public static final String FILE_NAME = "filename";

        public static final String AD_APP_ID = "ca-app-pub-2930251981891813~9153668281";


        public static final int NONE = 0;
        public static final int STAR_LIT_FILTER = 1;
        public static final int BLUE_MESS_FILTER = 2;
        public static final int AWE_STRUCK_VIBE_FILTER = 3;
        public static final int LIME_STUTTER_FILTER = 4;
        public static final int NIGHT_WHISPER_FILTER = 5;


        public static final String  NONE_ = "None";
        public static final String STAR_LIT_FILTER_ = "Star Lit";
        public static final String BLUE_MESS_FILTER_ = "Blue Mess";
        public static final String AWE_STRUCK_VIBE_FILTER_ = "Awe Struck Vibe";
        public static final String LIME_STUTTER_FILTER_ = "Lime Stutter";
        public static final String NIGHT_WHISPER_FILTER_ = "Night Whisper";


        public static final int WIDTH = 480;
        public static final int HEIGHT = 800;


        public static final int THUMBNAILS_WIDTH = 85;
        public static final int THUMBNAILS_HEIGHT = 100;

    }


    public static void tmsg(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }


    public static Bitmap MakeSquare(byte[] data, int cameraID) {
        int width;
        int height;
        Matrix matrix = new Matrix();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraID, info);
        // Convert ByteArray to Bitmap
        Bitmap bitPic = BitmapFactory.decodeByteArray(data, 0, data.length);
        width = bitPic.getWidth();
        height = bitPic.getHeight();

        // Perform matrix rotations/mirrors depending on camera that took the photo
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            float[] mirrorY = {-1, 0, 0, 0, 1, 0, 0, 0, 1};
            Matrix matrixMirrorY = new Matrix();
            matrixMirrorY.setValues(mirrorY);

            matrix.postConcat(matrixMirrorY);
        }

        matrix.postRotate(90);


        // Create new Bitmap out of the old one
        Bitmap bitPicFinal = Bitmap.createBitmap(bitPic, 0, 0, width, height, matrix, true);
        bitPic.recycle();
        int desWidth;
        int desHeight;
        desWidth = bitPicFinal.getWidth();
        desHeight = desWidth;
        Bitmap croppedBitmap = Bitmap.createBitmap(bitPicFinal, 0, bitPicFinal.getHeight() / 2 - bitPicFinal.getWidth() / 2, desWidth, desHeight);
        croppedBitmap = Bitmap.createScaledBitmap(croppedBitmap, 528, 528, true);
        return croppedBitmap;
    }


    public static void addFragmentToActivity(@NonNull FragmentManager manager,
                                             @NonNull Fragment fragment, @IdRes int frameId) {

        FragmentTransaction ft = manager.beginTransaction();
        ft.add(frameId, fragment);
        ft.commit();

    }

    public static void addFragmentToActivity(@NonNull FragmentManager manager,
                                             @NonNull Fragment fragment, @IdRes int frameId, @NonNull Bundle b) {

        fragment.setArguments(b);
        addFragmentToActivity(manager, fragment, frameId);

    }


    public static void recycleBitmaps(List<Bitmap> bitmapList) {

        try {

            for (int i = 0; i < bitmapList.size(); i++) {
                if (bitmapList.get(i) != null) {
                    bitmapList.get(i).recycle();
                    bitmapList.set(i, null);
                    Log.d("ClEAN UP - ", "recycleBitmaps: " + i + " Deleted");
                }
            }

        } catch (Exception e) {
            Log.e("CLEAN UP - ", "recycleBitmaps: ", e);
        }
    }


    public static Bitmap rescaleBitmap(Bitmap b, int w, int h) {
        return Bitmap.createScaledBitmap(b, w, h, false);
    }

    public static String generateUniqueName() {

        String a = UUID.randomUUID().toString();

        return "lumo-" + a + ".mp4";
    }

}

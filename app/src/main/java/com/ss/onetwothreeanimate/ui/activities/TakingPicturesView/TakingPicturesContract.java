package com.ss.onetwothreeanimate.ui.activities.TakingPicturesView;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;

import com.ss.onetwothreeanimate.BasePresenter;
import com.ss.onetwothreeanimate.BaseView;

import java.util.List;

/**
 * Created by Sammie on 3/6/2017.
 */

public interface TakingPicturesContract {

    interface view extends BaseView<presenter> {

        void OnCountdownFinish();

        void updateTimerValue(String s);

        void onFailedSavingInCache();

        void OnSavingInCacheSuccess(List<String> strings);
    }

    interface presenter extends BasePresenter {

        void onCaptureImage();

        void saveBitmapsInCache(List<Bitmap> bitmaps, FragmentActivity activity);
    }
}

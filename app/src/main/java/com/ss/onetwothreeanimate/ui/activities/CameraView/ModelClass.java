package com.ss.onetwothreeanimate.ui.activities.CameraView;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;


import com.ss.onetwothreeanimate.SequenceEnc;
import com.zomato.photofilters.SampleFilters;
import com.zomato.photofilters.geometry.Point;
import com.zomato.photofilters.imageprocessors.Filter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Sammie on 2/2/2017.
 */

class ModelClass {

    private static final String TAG = ModelClass.class.getName().toUpperCase();
    private Activity activity;

    private apiListener listener;

    ModelClass(Activity activity) {
        this.activity = activity;
    }


    void createMp4(List<Bitmap> bitmaps, Context context, apiListener listener) {
        this.listener = listener;

        Point[] rgbKnots;
        rgbKnots = new Point[3];
        rgbKnots[0] = new Point(0, 0);
        rgbKnots[1] = new Point(175, 139);
        rgbKnots[2] = new Point(255, 255);

        List<Bitmap> newBitmaps = new ArrayList<>();

        for (int i = 0; i < 5; i ++) {

            Filter myFilter = new Filter();
//            myFilter.addSubFilter(new VignetteSubfilter(context, 100));
//            myFilter.addSubFilter(new ToneCurveSubfilter(rgbKnots, null, null, null));
//            myFilter.addSubFilter(new ColorOverlaySubfilter(100, .2f, .2f, .0f));
//            myFilter.addSubFilter(new ContrastSubfilter(1.2f));
            Filter fooFilter = SampleFilters.getBlueMessFilter();
            newBitmaps.add(fooFilter.processFilter(bitmaps.get(i)));

        }

        bitmaps.clear();



        try {
            createMp4Task(newBitmaps);
        } catch (IOException e) {
            e.printStackTrace();
            listener.onError("Failed");
            listener.doCleanUp();
        }

    }


    private void createMp4Task(List<Bitmap> bitmaps) throws IOException {

        String path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM).toString();

        String filename = "lumofile.mp4";

        File file = new File(path, filename);

        if (file.exists()) {
            file.delete();
            file = new File(path, filename);
            Log.e("file exist", "" + file + ",Bitmap= " + filename);
        }

        SequenceEnc enc = new SequenceEnc(file);

        for (int i = 0; i < 5; i++) {
            enc.encodeImage(bitmaps.get(i));
        }

        enc.finish();
        listener.onSuccess(filename);
        listener.doCleanUp();


    }



    interface apiListener {
        void onSuccess(String s);

        void onError(String s);

        void doCleanUp();
    }


}

package com.ss.onetwothreeanimate.ui.activities.TakingPicturesView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.flurgle.camerakit.CameraKit;
import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;
import com.nineoldandroids.animation.Animator;
import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;
import com.ss.onetwothreeanimate.ui.activities.VideoPlayerView.VideoPlayerActivity;
import com.ss.onetwothreeanimate.ui.activities.VideoPlayerView.VideoPlayerFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Sammie on 3/6/2017.
 */

public class TakingPicturesFragment extends Fragment implements TakingPicturesContract.view {

    public static final String UNIQUE_FILENAME = "filename";

    @BindView(R.id.camera_fragment_cameraView)
    CameraView mCameraView;

    @BindView(R.id.camera_fragment_switchCamera)
    ImageView ivSwitchCamera;

    @BindView(R.id.camera_fragment_tvTimer)
    TextView tvTimer;

    @BindView(R.id.camera_fragment_imageView)
    ImageView mImageView;

    private int count = 0;

    private MediaPlayer mp;

    private List<Bitmap> bitmaps;

    private Unbinder unbinder;

    private TakingPicturesContract.presenter mPresenter;

    private ProgressDialog dialog;

    public void setPresenter(TakingPicturesContract.presenter presenter) {
        mPresenter = presenter;
    }

    public static TakingPicturesFragment newInstance() {
        return new TakingPicturesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bitmaps = new ArrayList<>();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void onResume() {
        super.onResume();
        mCameraView.start();

    }

    @Override
    public void onPause() {
        mCameraView.stop();
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.picture_taking_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        mCameraView.setCameraListener(cameraListener);
        mCameraView.setFocus(CameraKit.Constants.FOCUS_CONTINUOUS);
        mp = MediaPlayer.create(getContext(), R.raw.sound);

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");


        YoYo.with(Techniques.Shake)
                .playOn(mImageView);
    }


    CameraListener cameraListener = new CameraListener() {
        @Override
        public void onPictureTaken(byte[] jpeg) {
            super.onPictureTaken(jpeg);

            Bitmap b = BitmapFactory.decodeByteArray(jpeg, 0, jpeg.length);

            Bitmap result = Bitmap.createScaledBitmap(
                    b,
                    Utilz.AppConstant.WIDTH,
                    Utilz.AppConstant.HEIGHT,
                    false
            );


            if (count == 0) {

                bitmaps.add(result);
                mPresenter.onCaptureImage();
                count++;

            } else if (count == 1) {

                bitmaps.add(result);
                mPresenter.onCaptureImage();
                count++;

            } else if (count == 2) {

                bitmaps.add(result);
//                mPresenter.onCaptureImage();
                count++;

                tvTimer.setVisibility(View.GONE);
                dialog.show();
                mPresenter.saveBitmapsInCache(bitmaps, getActivity());

            }

//            else if (count == 3) {
//
//                bitmaps.add(result);
//                mPresenter.onCaptureImage();
//                count++;
//
//            } else if (count == 4) {
//
//                bitmaps.add(result);
//                count++;
//
//                tvTimer.setVisibility(View.GONE);
//                dialog.show();
//                mPresenter.saveBitmapsInCache(bitmaps, getActivity());
//            }


        }
    };


    @OnClick(R.id.camera_fragment_imageView)
    void onStartClick(View view) {

        ivSwitchCamera.setEnabled(false);

        mCameraView.setFocus(CameraKit.Constants.FOCUS_OFF);

        YoYo.with(Techniques.ZoomOut)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);

        mPresenter.onCaptureImage();
    }


    @OnClick(R.id.camera_fragment_switchCamera)
    void onCameraSwitchClick() {
        mCameraView.toggleFacing();
    }


    @Override
    public void OnCountdownFinish() {

        mCameraView.captureImage();
        mp.start();

    }

    @Override
    public void updateTimerValue(String s) {

        tvTimer.setText(s);
        YoYo.with(Techniques.FadeOut)
                .playOn(tvTimer);
    }

    @Override
    public void onFailedSavingInCache() {
        dialog.dismiss();
        Utilz.recycleBitmaps(bitmaps);
    }

    @Override
    public void OnSavingInCacheSuccess(List<String> strings) {

        dialog.dismiss();

        BitmapsFilenames obj = new BitmapsFilenames();
        obj.setListFilenames(strings);

        Intent i = new Intent(getActivity(), VideoPlayerActivity.class);
        i.putExtra(VideoPlayerFragment.BITMAP_OBJECT, obj);
        i.putExtra(UNIQUE_FILENAME, Utilz.generateUniqueName());
        startActivity(i);
        getActivity().finish();

        Utilz.recycleBitmaps(bitmaps);

    }
}

package com.ss.onetwothreeanimate.ui.activities.VideoPlayerView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.TaskRepository;
import com.ss.onetwothreeanimate.ui.activities.TakingPicturesView.TakingPicturesFragment;

/**
 * Created by Sammie on 3/2/2017.
 */

public class VideoPlayerActivity extends AppCompatActivity {


    private VideoPlayerPresenter mPresenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);


        VideoPlayerFragment fragment = (VideoPlayerFragment)
                getSupportFragmentManager().findFragmentById(R.id.activity_video_player_container);


        if (fragment == null)
            fragment = VideoPlayerFragment.newInstance();

        Bundle b = new Bundle();
        b.putParcelable(VideoPlayerFragment.BITMAP_OBJECT, getIntent().getParcelableExtra(VideoPlayerFragment.BITMAP_OBJECT));
        b.putString(TakingPicturesFragment.UNIQUE_FILENAME, getIntent().getStringExtra(TakingPicturesFragment.UNIQUE_FILENAME));

        Utilz.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.activity_video_player_container, b);


        //connect presenter with view and view with presenter
        mPresenter = new VideoPlayerPresenter(fragment, new TaskRepository());
        fragment.setPresenter(mPresenter);
    }
}

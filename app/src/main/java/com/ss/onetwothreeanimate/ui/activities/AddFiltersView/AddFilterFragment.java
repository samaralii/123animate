package com.ss.onetwothreeanimate.ui.activities.AddFiltersView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.ss.onetwothreeanimate.Listeners;
import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.adapters.ImagesAdapter;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;
import com.ss.onetwothreeanimate.data.entities.FilteredBitmapsPojo;
import com.ss.onetwothreeanimate.ui.activities.TakingPicturesView.TakingPicturesFragment;
import com.ss.onetwothreeanimate.ui.activities.VideoPlayerView.VideoPlayerFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by Sammie on 3/3/2017.
 */

public class AddFilterFragment extends Fragment implements AddFilterContract.view, Listeners.imagesListCallback {

    private AddFilterContract.presenter mPresenter;

    private Unbinder unbinder;

    private BitmapsFilenames bitmapsFilenames;

    private int filter;

    private ProgressDialog dialog;

    @BindView(R.id.add_filters_imageView)
    ImageView imageView;

    @BindView(R.id.add_filters_fragment_list)
    RecyclerView recyclerView;
    private String fileName;

    public static AddFilterFragment newInstance() {
        return new AddFilterFragment();
    }

    @Override
    public void setPresenter(AddFilterContract.presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().getParcelable(VideoPlayerFragment.BITMAP_OBJECT) == null)
            Utilz.tmsg(getActivity(), "Error");
        else
            bitmapsFilenames = getArguments().getParcelable(VideoPlayerFragment.BITMAP_OBJECT);

        fileName = getArguments().getString(TakingPicturesFragment.UNIQUE_FILENAME);

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Creating Video...");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_filters_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);

        init();

    }

    private void init() {

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager ll = new LinearLayoutManager(getActivity());
        ll.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(ll);
        mPresenter.createThumbnails(getContext(), bitmapsFilenames);

        mPresenter.newApplyFilter(getContext(), Utilz.AppConstant.NONE_, bitmapsFilenames);
    }


    @OnClick(R.id.add_filters_btnApply)
    void onApplyFilterOnVideo() {
        dialog.show();
        mPresenter.applyFilterOnVideo(getContext(), filter, bitmapsFilenames, fileName);
    }


    @OnClick(R.id.add_filters_btnCancel)
    void onCancelCLick() {
        getActivity().finish();
    }


    @Override
    public void showPreview(Bitmap bitmap, int filter) {

        this.filter = filter;

        YoYo.with(Techniques.FadeIn)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        imageView.setImageBitmap(bitmap);
                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {}
                    @Override
                    public void onAnimationCancel(Animator animation) {}
                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                })
                .playOn(imageView);

    }

    @Override
    public void onVideoSuccessfullyCreated(String fileName) {
        dialog.dismiss();
        Intent i = new Intent();
        i.putExtra(VideoPlayerFragment.FILE_NAME, fileName);
        getActivity().setResult(getActivity().RESULT_OK, i);
        getActivity().finish();
    }

    @Override
    public void onVideoFailed() {
        dialog.dismiss();
        Utilz.tmsg(getActivity(), "Error While Creating Video");
    }

    @Override
    public void ErrorAtShowingPreview() {
        Utilz.tmsg(getActivity(), "Error No Preview");
    }

    @Override
    public void filteredBitmapsList(List<FilteredBitmapsPojo> value) {
        ImagesAdapter adapter = new ImagesAdapter(value, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onFilteredBitmapsListError() {
        Utilz.tmsg(getActivity(), "Error Showing Thumbnails");
    }


    @Override
    public void onClick(String filterName) {
        mPresenter.newApplyFilter(getContext(), filterName, bitmapsFilenames);
    }


}

package com.ss.onetwothreeanimate.ui.activities.AddFiltersView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.TaskRepository;
import com.ss.onetwothreeanimate.ui.activities.TakingPicturesView.TakingPicturesFragment;
import com.ss.onetwothreeanimate.ui.activities.VideoPlayerView.VideoPlayerFragment;

/**
 * Created by Sammie on 3/2/2017.
 */

public class AddFilterActivity extends AppCompatActivity {

    private AddFilterPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_filters);

        AddFilterFragment fragment  =
                (AddFilterFragment) getSupportFragmentManager().findFragmentById(R.id.activity_add_filters_container);


        if (fragment == null)
            fragment = AddFilterFragment.newInstance();


        Bundle b = new Bundle();
        b.putParcelable(VideoPlayerFragment.BITMAP_OBJECT, getIntent().getParcelableExtra(VideoPlayerFragment.BITMAP_OBJECT));
        b.putString(TakingPicturesFragment.UNIQUE_FILENAME, getIntent().getStringExtra(TakingPicturesFragment.UNIQUE_FILENAME));

        Utilz.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.activity_add_filters_container, b);


        mPresenter = new AddFilterPresenter(fragment, new TaskRepository());
        fragment.setPresenter(mPresenter);


    }


}

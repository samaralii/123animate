package com.ss.onetwothreeanimate.ui.activities.TakingPicturesView;

import android.graphics.Bitmap;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.TasksDataSource;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sammie on 3/6/2017.
 */

public class TakingPicturesPresenter implements TakingPicturesContract.presenter {

    @NonNull
    private final TakingPicturesContract.view mView;

    @NonNull
    private final TasksDataSource mTaskDataSource;

    private int secondsLeft = 0;

    private CompositeDisposable disposable = new CompositeDisposable();


    public TakingPicturesPresenter(@NonNull TakingPicturesContract.view mView, @NonNull TasksDataSource mTaskDataSource) {
        this.mView = mView;
        this.mTaskDataSource = mTaskDataSource;
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }


    @Override
    public void onCaptureImage() {

        new CountDownTimer(2000, 100) {
            @Override
            public void onTick(long ms) {

                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) ms / 1000.0f);
                    mView.updateTimerValue(secondsLeft + "");
                }

            }

            @Override
            public void onFinish() {
                mView.OnCountdownFinish();
            }
        }.start();

    }

    @Override
    public void saveBitmapsInCache(List<Bitmap> bitmaps, FragmentActivity activity) {

        disposable.add(mTaskDataSource.saveBitmapsInCacheReactively(bitmaps, activity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> Utilz.recycleBitmaps(bitmaps))
                .subscribeWith(new DisposableSingleObserver<List<String>>() {
                    @Override
                    public void onSuccess(List<String> value) {
                        mView.OnSavingInCacheSuccess(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        mView.onFailedSavingInCache();
                    }
                }));


    }

}

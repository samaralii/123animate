package com.ss.onetwothreeanimate.ui.activities.AddFiltersView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;

import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.TasksDataSource;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;
import com.ss.onetwothreeanimate.data.entities.FilteredBitmapsPojo;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sammie on 3/3/2017.
 */

class AddFilterPresenter implements AddFilterContract.presenter {

    @NonNull
    private final AddFilterContract.view mView;


    @NonNull
    private final TasksDataSource mTaskDataSource;

    private int filter = 0;


    private CompositeDisposable disposable = new CompositeDisposable();

    public AddFilterPresenter(@NonNull AddFilterContract.view mView, @NonNull TasksDataSource mTaskDataSource) {
        this.mView = mView;
        this.mTaskDataSource = mTaskDataSource;
    }


    public void subscribe() {}

    @Override
    public void unSubscribe() {
        disposable.clear();
    }

    @Override
    public void applyFilterOnVideo(Context context, int filter, BitmapsFilenames bitmapsFilenames, String videoFilename) {

        List<Bitmap> mutableBitmap = new ArrayList<>();

        try {

            for (int i = 0; i < bitmapsFilenames.getListFilenames().size(); i++) {

                Bitmap bitmap = BitmapFactory.decodeStream(context
                        .openFileInput(bitmapsFilenames.getListFilenames().get(i)));

                mutableBitmap.add(bitmap.copy(Bitmap.Config.ARGB_8888, true));

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mView.onVideoFailed();
        }


        disposable.add(mTaskDataSource.applyFiltersAndCreateVideoReactively(mutableBitmap, filter, videoFilename)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally(() -> Utilz.recycleBitmaps(mutableBitmap))
                .subscribeWith(new DisposableSingleObserver<String>() {
                    @Override
                    public void onSuccess(String value) {
                        mView.onVideoSuccessfullyCreated(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        mView.onVideoFailed();
                    }
                }));

    }

    @Override
    public void createThumbnails(Context context, BitmapsFilenames bitmapsFilenames) {

        List<Bitmap> bitmapList = new ArrayList<>();

        try {

            for (int i = 0; i < 6; i++) {

                Bitmap bitmap = BitmapFactory.decodeStream(context
                        .openFileInput(bitmapsFilenames.getListFilenames().get(0)));

                bitmapList.add(Utilz.rescaleBitmap(bitmap,
                        Utilz.AppConstant.THUMBNAILS_WIDTH,
                        Utilz.AppConstant.THUMBNAILS_HEIGHT));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mView.ErrorAtShowingPreview();
        }


        disposable.add(mTaskDataSource.applyFiltersOnThumbnails(bitmapList)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<FilteredBitmapsPojo>>() {
                    @Override
                    public void onSuccess(List<FilteredBitmapsPojo> value) {
                        mView.filteredBitmapsList(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        mView.onFilteredBitmapsListError();
                    }
                }));


    }

    @Override
    public void newApplyFilter(Context context, String filterName, BitmapsFilenames bitmapsFilenames) {

        Bitmap mutableBitmap = null;

        try {

            Bitmap bitmap = BitmapFactory.decodeStream(context
                    .openFileInput(bitmapsFilenames.getListFilenames().get(0)));


            mutableBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            mView.ErrorAtShowingPreview();
        }

        switch (filterName) {
            case Utilz.AppConstant.BLUE_MESS_FILTER_:
                filter = Utilz.AppConstant.BLUE_MESS_FILTER;
                break;

            case Utilz.AppConstant.AWE_STRUCK_VIBE_FILTER_:
                filter = Utilz.AppConstant.AWE_STRUCK_VIBE_FILTER;
                break;

            case Utilz.AppConstant.NIGHT_WHISPER_FILTER_:
                filter = Utilz.AppConstant.NIGHT_WHISPER_FILTER;
                break;

            case Utilz.AppConstant.STAR_LIT_FILTER_:
                filter = Utilz.AppConstant.STAR_LIT_FILTER;
                break;

            case Utilz.AppConstant.LIME_STUTTER_FILTER_:
                filter = Utilz.AppConstant.LIME_STUTTER_FILTER;
                break;

            case Utilz.AppConstant.NONE_:
                filter = Utilz.AppConstant.NONE;
                break;


        }


        if (filter == Utilz.AppConstant.NONE)
            mView.showPreview(mutableBitmap, 0);
        else {

            disposable.add(mTaskDataSource.applyFilterOnBitmapReactively(mutableBitmap, filter)
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<Bitmap>() {
                        @Override
                        public void onSuccess(Bitmap value) {
                            mView.showPreview(value, filter);
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            mView.ErrorAtShowingPreview();
                        }
                    }));
        }


    }


}

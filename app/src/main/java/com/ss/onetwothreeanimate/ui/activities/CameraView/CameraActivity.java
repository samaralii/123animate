package com.ss.onetwothreeanimate.ui.activities.CameraView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;
import com.ss.onetwothreeanimate.listeners.CameraFragmentListener;
import com.ss.onetwothreeanimate.ui.activities.VideoPlayerView.VideoPlayerActivity;
import com.ss.onetwothreeanimate.ui.activities.VideoPlayerView.VideoPlayerFragment;
import com.ss.onetwothreeanimate.view.CameraFragment;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sammie on 2/2/2017.
 */

public class CameraActivity extends AppCompatActivity implements CameraFragmentListener {
    public static final String TAG = CameraActivity.class.getName().toUpperCase();


    @BindView(R.id.imageview)
    ImageView imageView;

    @BindView(R.id.imageview2)
    ImageView imageView2;

    @BindView(R.id.imageview3)
    ImageView imageView3;

    @BindView(R.id.imageview4)
    ImageView imageView4;

    @BindView(R.id.imageview5)
    ImageView imageView5;


    @BindView(R.id.tvTimer)
    TextView tvTimer;


    @BindView(R.id.activity_camera_image)
    ImageView mainImage;


    int count = 0;

    private MediaPlayer mp;

    int secondsLeft = 0;

    private ModelClass model;
    private CameraFragment fragment;
    private View switchCameraView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        System.loadLibrary("NativeImageProcessor");

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        ButterKnife.bind(this);
        model = new ModelClass(this);

        mp = MediaPlayer.create(this, R.raw.sound);
        init();

    }

    private void init() {

        fragment = (CameraFragment) getSupportFragmentManager().findFragmentById(
                R.id.camera_fragment
        );

        switchCameraView = findViewById(R.id.camera_fragment_switchCamera);

        YoYo.with(Techniques.Shake)
                .playOn(mainImage);

    }

    @OnClick(R.id.camera_fragment_switchCamera)
    void onSwitchCameraClick() {
        fragment.switchCamera();
    }


    @Override
    public void onCameraError() {
        Utilz.tmsg(this, "Error");
    }

    @OnClick(R.id.activity_camera_image)
    public void takePicture(View view) {

        YoYo.with(Techniques.ZoomOut)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(view);


        takepic();
    }


    private void takepic() {

        switchCameraView.setEnabled(false);


        new CountDownTimer(2000, 100) {

            @Override
            public void onTick(long ms) {

                if (Math.round((float) ms / 1000.0f) != secondsLeft) {
                    secondsLeft = Math.round((float) ms / 1000.0f);
                    tvTimer.setText("" + secondsLeft);

                    YoYo.with(Techniques.FadeOut)
                            .playOn(tvTimer);
                }

            }

            @Override
            public void onFinish() {

                try {
                    fragment.takePicture();
                    mp.start();
                } catch (Exception e) {
                    Log.e("TAG", "onFinish: ", e);
                }
            }
        }.start();


    }

    private List<Bitmap> bitmapList = new ArrayList<>();

    public void onPictureTaken(Bitmap bitmap) {


        if (count == 0) {

            bitmapList.add(bitmap);
            count++;
            takepic();

        } else if (count == 1) {

            bitmapList.add(bitmap);
            count++;
            takepic();

        } else if (count == 2) {

            bitmapList.add(bitmap);
            count++;
            takepic();

        } else if (count == 3) {

            bitmapList.add(bitmap);
            count++;
            takepic();

        } else if (count == 4) {

            bitmapList.add(bitmap);
            count++;
            tvTimer.setVisibility(View.INVISIBLE);


            Observable<List<String>> observable = saveBitmapInCache(bitmapList);

            ProgressDialog dialog = ProgressDialog.show(this, "", "Loading...");


            observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(throwable -> {

                        Log.e(TAG, "onPictureTaken: ", throwable);
                        Utilz.tmsg(this, "Error");
                        finish();
                        startActivity(getIntent());
                        dialog.dismiss();

                        try {
                            recycleBitmaps();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    })
                    .subscribe(data -> {

                        BitmapsFilenames obj = new BitmapsFilenames();
                        obj.setListFilenames(data);

                        Intent i = new Intent(CameraActivity.this, VideoPlayerActivity.class);
                        i.putExtra(VideoPlayerFragment.BITMAP_OBJECT, obj);
                        startActivity(i);
                        finish();
                        dialog.dismiss();

                        try {
                            recycleBitmaps();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    });





        }


    }

    private Observable<List<String>> saveBitmapInCache(List<Bitmap> list) {
        return Observable.create(e -> {


            List<String> stringList = new ArrayList<>(5);

            for (int i = 0; i < 5; i++) {
                String fileName = Utilz.AppConstant.BITMAP_FILENAME + "_" + i;
                Bitmap bitmap = list.get(i);

                try {

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);


                    fo.write(bytes.toByteArray());
                    // remember close file output
                    fo.close();

                    stringList.add(fileName);

                } catch (Exception ex) {

                    ex.printStackTrace();
                    fileName = null;
                    e.onError(ex);

                }


            }

            e.onNext(stringList);


        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            recycleBitmaps();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void recycleBitmaps() throws Exception {
        for (int i = 0; i < 5; i++) {
            if (bitmapList.get(i) != null) {
                bitmapList.get(i).recycle();
                bitmapList.set(i, null);
                Log.d(TAG, "recycleBitmaps: " + i + " Deleted");
            }
        }
    }


}

package com.ss.onetwothreeanimate.ui.activities.VideoPlayerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;

import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.TasksDataSource;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sammie on 3/2/2017.
 */

class VideoPlayerPresenter implements VideoPlayerContract.presenter{



    private CompositeDisposable disposable = new CompositeDisposable();

    @NonNull
    private final VideoPlayerContract.view mView;

    @NonNull
    private final TasksDataSource mTasksDataSource;

    VideoPlayerPresenter(@NonNull VideoPlayerContract.view mView, @NonNull TasksDataSource mTasksDataSource) {
        this.mView = mView;
        this.mTasksDataSource = mTasksDataSource;
    }


    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        disposable.clear();
    }


    @Override
    public void createVideo(BitmapsFilenames bitmapsFilenames, Context context, String Videofilename) {
        //TODO check for nulls and bitmaps

        List<Bitmap> bitmapList = new ArrayList<>();

        try {


            for (int i = 0; i < bitmapsFilenames.getListFilenames().size(); i++) {

                String fileName = Utilz.AppConstant.BITMAP_FILENAME + "_" + i;

                Bitmap bitmap = BitmapFactory.decodeStream(context
                        .openFileInput(fileName));

                bitmapList.add(bitmap);

            }


        } catch (Exception e) {
            e.printStackTrace();
            mView.onFailedCreatingVideo();
        }


       disposable.add(mTasksDataSource.createVideoFromBitmapsReactively(bitmapList, Videofilename)
               .subscribeOn(Schedulers.computation())
               .observeOn(AndroidSchedulers.mainThread())
               .doFinally(() -> Utilz.recycleBitmaps(bitmapList))
               .subscribeWith(new DisposableSingleObserver<String>() {
                   @Override
                   public void onSuccess(String value) {
                       mView.onSuccessfullyVideoCreated(getUri(value));
                   }

                   @Override
                   public void onError(Throwable e) {
                       mView.onFailedCreatingVideo();
                   }
               }));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (requestCode == VideoPlayerFragment.ADD_FILTERS_REQUEST_CODE) {

            String fileName = data.getStringExtra(VideoPlayerFragment.FILE_NAME);

            if (fileName == null || fileName.isEmpty())
                mView.onActivityResultFailed();

            mView.onSuccessfullyVideoCreated(getUri(fileName));

        }

    }


    private Uri getUri(String fileName) {

        String path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM).toString();

        File file = new File(path, fileName);

        return Uri.fromFile(file);
    }

}

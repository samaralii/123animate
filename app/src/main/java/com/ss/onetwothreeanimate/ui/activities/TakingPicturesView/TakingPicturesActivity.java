package com.ss.onetwothreeanimate.ui.activities.TakingPicturesView;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.TaskRepository;

/**
 * Created by Sammie on 3/6/2017.
 */


public class TakingPicturesActivity extends AppCompatActivity {


    static {
        System.loadLibrary("NativeImageProcessor");

    }

    private TakingPicturesPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taking_pictures);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        TakingPicturesFragment fragment =
                (TakingPicturesFragment) getSupportFragmentManager().findFragmentById(R.id.activity_taking_pictures_container);


        if (fragment == null)
            fragment = TakingPicturesFragment.newInstance();

        Utilz.addFragmentToActivity(getSupportFragmentManager(), fragment, R.id.activity_taking_pictures_container);

        mPresenter = new TakingPicturesPresenter(fragment, new TaskRepository());
        fragment.setPresenter(mPresenter);


        askForPermission();

    }

    private void askForPermission() {


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


}

package com.ss.onetwothreeanimate.ui.activities.AddFiltersView;

import android.content.Context;
import android.graphics.Bitmap;

import com.ss.onetwothreeanimate.BasePresenter;
import com.ss.onetwothreeanimate.BaseView;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;
import com.ss.onetwothreeanimate.data.entities.FilteredBitmapsPojo;

import java.util.List;

/**
 * Created by Sammie on 3/3/2017.
 */

public interface AddFilterContract {

    interface view extends BaseView<presenter> {
        void showPreview(Bitmap bitmap, int filter);

        void onVideoSuccessfullyCreated(String fileName);

        void onVideoFailed();

        void ErrorAtShowingPreview();

        void filteredBitmapsList(List<FilteredBitmapsPojo> value);

        void onFilteredBitmapsListError();
    }

    interface presenter extends BasePresenter{

        void applyFilterOnVideo(Context context, int filter, BitmapsFilenames bitmapsFilenames, String videoFilename);

        void createThumbnails(Context context, BitmapsFilenames bitmapsFilenames);

        void newApplyFilter(Context context, String filterName, BitmapsFilenames bitmapsFilenames);
    }

}

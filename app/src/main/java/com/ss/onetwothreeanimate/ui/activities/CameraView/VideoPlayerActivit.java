package com.ss.onetwothreeanimate.ui.activities.CameraView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.MediaController;
import android.widget.VideoView;

import com.ss.onetwothreeanimate.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Sammie on 2/16/2017.
 */

public class VideoPlayerActivit extends AppCompatActivity {


    private static final String FILE_NAME = "filename";

    @BindView(R.id.vid)
    VideoView mVideoView;
    private Uri uri;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_activity);
        ButterKnife.bind(this);
        init();
    }

    private void init() {


        String path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM).toString();

        String filename = getIntent().getStringExtra(FILE_NAME);

        File file = new File(path, filename);


        uri = Uri.fromFile(file);

        mVideoView.setMediaController(new MediaController(this));
        mVideoView.setVideoURI(uri);
        mVideoView.requestFocus();
        mVideoView.start();

        mVideoView.setOnPreparedListener(mp -> mp.setLooping(true));


    }


    @OnClick(R.id.gif_activity_btnShare)
    void onShareClick() {


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sendIntent.setType("video/mp4");
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));


    }


    @OnClick(R.id.gif_activity_btnBack)
    void onBackClick() {

        startActivity(new Intent(this, CameraActivity.class));
        finish();

    }
}

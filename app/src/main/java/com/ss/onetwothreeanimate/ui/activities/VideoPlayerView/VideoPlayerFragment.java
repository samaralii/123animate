package com.ss.onetwothreeanimate.ui.activities.VideoPlayerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;
import com.ss.onetwothreeanimate.ui.activities.AddFiltersView.AddFilterActivity;
import com.ss.onetwothreeanimate.ui.activities.TakingPicturesView.TakingPicturesActivity;
import com.ss.onetwothreeanimate.ui.activities.TakingPicturesView.TakingPicturesFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by Sammie on 3/2/2017.
 */

public class VideoPlayerFragment extends Fragment implements VideoPlayerContract.view {

    public static final String FILE_NAME = "filename";
    public static final String BITMAP_OBJECT = "bitmap_object";
    public static final int ADD_FILTERS_REQUEST_CODE = 1;


    private Unbinder unbinder;

    private VideoPlayerContract.presenter mPresenter;

    @BindView(R.id.video_fragment_vid)
    VideoView mVideoView;


    @BindView(R.id.ad_view)
    AdView adView;


    private String filename;
    private Uri uri;


    private ProgressDialog dialog;

    private BitmapsFilenames bitmapsFilenames;

    private String videoFilename;

    public VideoPlayerFragment() {

    }

    @Override
    public void setPresenter(VideoPlayerContract.presenter presenter) {
        mPresenter = presenter;
    }

    public static VideoPlayerFragment newInstance() {
        return new VideoPlayerFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.subscribe();

        if (mVideoView != null)
            if (!mVideoView.isPlaying())
                mVideoView.start();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.unSubscribe();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().getParcelable(BITMAP_OBJECT) == null)
            Utilz.tmsg(getActivity(), "Error");
        else
            bitmapsFilenames = getArguments().getParcelable(BITMAP_OBJECT);


        videoFilename = getArguments().getString(TakingPicturesFragment.UNIQUE_FILENAME);


        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Creating Video...");

        dialog.show();
        mPresenter.createVideo(bitmapsFilenames, getActivity(), videoFilename);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.video_player_fragment, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        loadAd();
    }

    private void loadAd() {

        MobileAds.initialize(getContext(), Utilz.AppConstant.AD_APP_ID);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("B8B00C9964EA836EBAAE4F9B04A5FB9A") //my nexus
                .build();


        adView.loadAd(adRequest);


    }


    @OnClick(R.id.video_fragment_btnAddFilters)
    void OnBtnAddFiltersClick() {

        Intent i = new Intent(getContext(), AddFilterActivity.class);
        i.putExtra(BITMAP_OBJECT, bitmapsFilenames);
        i.putExtra(TakingPicturesFragment.UNIQUE_FILENAME, videoFilename);
        startActivityForResult(i, ADD_FILTERS_REQUEST_CODE);

    }

    @OnClick(R.id.video_fragment_btnShare)
    void OnBtnShareClick() {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
        sendIntent.setType("video/mp4");
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));

    }

    @OnClick(R.id.video_fragment_btnBack)
    void OnBtnBackClick() {

        startActivity(new Intent(getActivity(), TakingPicturesActivity.class));
        getActivity().finish();

    }


    @Override
    public void onSuccessfullyVideoCreated(Uri uri) {

        dialog.dismiss();
        this.uri = uri;

        mVideoView.setVideoURI(uri);
        mVideoView.requestFocus();
        mVideoView.start();
        mVideoView.setOnPreparedListener(mp -> mp.setLooping(true));


    }

    @Override
    public void onActivityResultFailed() {
        Utilz.tmsg(getActivity(), "Error");
    }

    @Override
    public void onFailedCreatingVideo() {
        dialog.dismiss();
        Utilz.tmsg(getActivity(), "Error Creating Video");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPresenter.onActivityResult(requestCode, resultCode, data);
    }
}

package com.ss.onetwothreeanimate.ui.activities.VideoPlayerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.ss.onetwothreeanimate.BasePresenter;
import com.ss.onetwothreeanimate.BaseView;
import com.ss.onetwothreeanimate.data.entities.BitmapsFilenames;

/**
 * Created by Sammie on 3/2/2017.
 */

public interface VideoPlayerContract {

    interface view extends BaseView<presenter> {

        void onSuccessfullyVideoCreated(Uri uri);

        void onActivityResultFailed();

        void onFailedCreatingVideo();
    }

    interface presenter extends BasePresenter {

        void createVideo(BitmapsFilenames bitmapsFilenames, Context context, String Videofilename);

        void onActivityResult(int requestCode, int resultCode, Intent data);

    }


}

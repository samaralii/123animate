package com.ss.onetwothreeanimate;

import android.graphics.Bitmap;

import org.jcodec.scale.BitmapUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by Sammie on 2/17/2017.
 */

public class SequenceEnc extends SeqEnc{

    public SequenceEnc(File out) throws IOException {
        super(out);
    }

    public void encodeImage(Bitmap bi) throws IOException {
        encodeNativeFrame(BitmapUtil.fromBitmap(bi));
    }
}

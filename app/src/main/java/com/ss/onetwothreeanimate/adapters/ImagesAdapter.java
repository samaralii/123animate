package com.ss.onetwothreeanimate.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ss.onetwothreeanimate.Listeners;
import com.ss.onetwothreeanimate.R;
import com.ss.onetwothreeanimate.data.entities.FilteredBitmapsPojo;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sammie on 3/13/2017.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImagesAdapterViewHolder> {

    private List<FilteredBitmapsPojo> data;
    private Listeners.imagesListCallback listeners;

    public ImagesAdapter(List<FilteredBitmapsPojo> data, Listeners.imagesListCallback listeners) {
        this.data = data;
        this.listeners = listeners;
    }

    @Override
    public ImagesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_images, parent, false);
        return new ImagesAdapterViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImagesAdapterViewHolder holder, int position) {

        try {
            holder.imageView.setImageBitmap(data.get(position).getBitmap());
            holder.textView.setText(data.get(position).getFilterName());
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ImagesAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.list_images_imageView)
        ImageView imageView;

        @BindView(R.id.list_images_filterName)
        TextView textView;

        ImagesAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listeners.onClick(data.get(getLayoutPosition()).getFilterName());
        }
    }
}

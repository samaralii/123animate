package com.ss.onetwothreeanimate.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.ss.onetwothreeanimate.SequenceEnc;
import com.ss.onetwothreeanimate.Utilz;
import com.ss.onetwothreeanimate.data.entities.FilteredBitmapsPojo;
import com.zomato.photofilters.SampleFilters;
import com.zomato.photofilters.imageprocessors.Filter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.reactivex.Single;

/**
 * Created by Sammie on 3/3/2017.
 */

public class TaskRepository implements TasksDataSource {


    @Override
    public Single<List<String>> saveBitmapsInCacheReactively(List<Bitmap> list, FragmentActivity activity) {
        return Single.fromCallable(() -> saveBitmaps(list, activity));
    }

    @Override
    public Single<String> createVideoFromBitmapsReactively(List<Bitmap> bitmapList, String fileName) {
        return Single.fromCallable(() -> newCreateVideo(bitmapList, fileName));
    }

    @Override
    public Single<Bitmap> applyFilterOnBitmapReactively(Bitmap bitmap, int filter) {
        return Single.fromCallable(() -> newApplyFilterOnBitmap(bitmap, filter));
    }

    @Override
    public Single<String> applyFiltersAndCreateVideoReactively(List<Bitmap> mutableBitmap, int filter, String fileName) {
        return Single.fromCallable(() -> newApplyFilterAndCreateVideo(mutableBitmap, filter, fileName));
    }

    @Override
    public Single<List<FilteredBitmapsPojo>> applyFiltersOnThumbnails(List<Bitmap> bitmapList) {
        return Single.fromCallable(() -> applyFilterOnThumbnailsMethod(bitmapList));
    }


    private List<FilteredBitmapsPojo> applyFilterOnThumbnailsMethod(List<Bitmap> bitmapList) {

        List<FilteredBitmapsPojo> list = new ArrayList<>();


        list.add(new FilteredBitmapsPojo(bitmapList.get(0), "None"));


        Filter filter1 = SampleFilters.getBlueMessFilter();
        list.add(new FilteredBitmapsPojo(filter1.processFilter(bitmapList.get(1)), "Blue Mess"));

        Filter filter2 = SampleFilters.getAweStruckVibeFilter();
        list.add(new FilteredBitmapsPojo(filter2.processFilter(bitmapList.get(2)), "Awe Struck Vibe"));


        Filter filter3 = SampleFilters.getLimeStutterFilter();
        list.add(new FilteredBitmapsPojo(filter3.processFilter(bitmapList.get(3)), "Lime Stutter"));


        Filter filter4 = SampleFilters.getNightWhisperFilter();
        list.add(new FilteredBitmapsPojo(filter4.processFilter(bitmapList.get(4)), "Night Whisper"));


        Filter filter5 = SampleFilters.getStarLitFilter();
        list.add(new FilteredBitmapsPojo(filter5.processFilter(bitmapList.get(5)), "Star Lit"));


        return list;
    }

    private String newApplyFilterAndCreateVideo(List<Bitmap> mutableBitmap, int filter, String fileName) {

        Filter fooFilter = null;
        List<Bitmap> filteredBitmaps = new ArrayList<>(5);

        if (filter == Utilz.AppConstant.NONE) {
            return newCreateVideo(mutableBitmap, fileName);
        }

        switch (filter) {

            case Utilz.AppConstant.BLUE_MESS_FILTER:
                fooFilter = SampleFilters.getBlueMessFilter();
                break;

            case Utilz.AppConstant.AWE_STRUCK_VIBE_FILTER:
                fooFilter = SampleFilters.getAweStruckVibeFilter();
                break;

            case Utilz.AppConstant.LIME_STUTTER_FILTER:
                fooFilter = SampleFilters.getLimeStutterFilter();
                break;

            case Utilz.AppConstant.NIGHT_WHISPER_FILTER:
                fooFilter = SampleFilters.getNightWhisperFilter();
                break;

            case Utilz.AppConstant.STAR_LIT_FILTER:
                fooFilter = SampleFilters.getStarLitFilter();
                break;
        }

        for (int i = 0; i < mutableBitmap.size(); i++) {
            if (fooFilter != null) {
                filteredBitmaps.add(fooFilter.processFilter(mutableBitmap.get(i)));
            }
        }

        return newCreateVideo(filteredBitmaps, fileName);

    }


    private Bitmap newApplyFilterOnBitmap(Bitmap bitmap, int filter) {

        Filter fooFilter;
        Bitmap b = null;

        switch (filter) {

            case Utilz.AppConstant.BLUE_MESS_FILTER:

                fooFilter = SampleFilters.getBlueMessFilter();
                b = fooFilter.processFilter(bitmap);
                break;

            case Utilz.AppConstant.AWE_STRUCK_VIBE_FILTER:

                fooFilter = SampleFilters.getAweStruckVibeFilter();
                b = fooFilter.processFilter(bitmap);
                break;

            case Utilz.AppConstant.LIME_STUTTER_FILTER:

                fooFilter = SampleFilters.getLimeStutterFilter();
                b = fooFilter.processFilter(bitmap);
                break;

            case Utilz.AppConstant.NIGHT_WHISPER_FILTER:

                fooFilter = SampleFilters.getNightWhisperFilter();
                b = fooFilter.processFilter(bitmap);
                break;

            case Utilz.AppConstant.STAR_LIT_FILTER:

                fooFilter = SampleFilters.getStarLitFilter();
                b = fooFilter.processFilter(bitmap);
                break;
        }

        return b;

    }


    private String newCreateVideo(List<Bitmap> bitmaps, String fileName) {

        try {

            String path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM).toString();

//            filename = generateUniqueName();

            File file = new File(path, fileName);

            if (file.exists()) {
                file.delete();
                file = new File(path, fileName);
                Log.e("file exist", "" + file + ",Bitmap= " + fileName);
            }

            SequenceEnc enc = new SequenceEnc(file);

            for (int i = 0; i < bitmaps.size(); i++) {
                enc.encodeImage(bitmaps.get(i));
            }

            for (int i = 0; i < bitmaps.size(); i++) {
                enc.encodeImage(bitmaps.get(i));
            }

            enc.finish();

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            recycleBitmaps(bitmaps);
        }

        return fileName;

    }


    private void recycleBitmaps(List<Bitmap> bitmapList) {

        try {

            for (int i = 0; i < bitmapList.size(); i++) {
                if (bitmapList.get(i) != null) {
                    bitmapList.get(i).recycle();
                    bitmapList.set(i, null);
                    Log.d("ClEAN UP - ", "recycleBitmaps: " + i + " Deleted");
                }
            }

        } catch (Exception e) {
            Log.e("CLEAN UP - ", "recycleBitmaps: ", e);
        }


    }


    private List<String> saveBitmaps(List<Bitmap> list, FragmentActivity activity) {


        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            String fileName = Utilz.AppConstant.BITMAP_FILENAME + "_" + i;
            Bitmap bitmap = list.get(i);

            try {

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                FileOutputStream fo = activity.openFileOutput(fileName, Context.MODE_PRIVATE);


                fo.write(bytes.toByteArray());
                fo.close();

                stringList.add(fileName);

            } catch (Exception ex) {
                ex.printStackTrace();
                fileName = null;
            }


        }

        recycleBitmaps(list);
        return stringList;
    }


}

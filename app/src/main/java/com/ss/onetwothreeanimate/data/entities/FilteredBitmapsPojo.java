package com.ss.onetwothreeanimate.data.entities;

import android.graphics.Bitmap;

/**
 * Created by Sammie on 3/13/2017.
 */

public class FilteredBitmapsPojo {

    private Bitmap bitmap;
    private String filterName;

    public FilteredBitmapsPojo(Bitmap bitmap, String filterName) {
        this.bitmap = bitmap;
        this.filterName = filterName;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getFilterName() {
        return filterName;
    }


}

package com.ss.onetwothreeanimate.data;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;

import com.ss.onetwothreeanimate.data.entities.FilteredBitmapsPojo;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by Sammie on 3/3/2017.
 */

public interface TasksDataSource {

    Single<List<String>> saveBitmapsInCacheReactively(List<Bitmap> list, FragmentActivity activity);

    Single<String> createVideoFromBitmapsReactively(List<Bitmap> bitmapList, String fileName);

    Single<Bitmap> applyFilterOnBitmapReactively(Bitmap bitmap, int filter);

    Single<String> applyFiltersAndCreateVideoReactively(List<Bitmap> mutableBitmap, int filter, String fileName);

    Single<List<FilteredBitmapsPojo>> applyFiltersOnThumbnails(List<Bitmap> bitmapList);

}

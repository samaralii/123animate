package com.ss.onetwothreeanimate.data.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sammie on 3/3/2017.
 */

public class BitmapsFilenames implements Parcelable {

    private List<String> listFilenames;


    public BitmapsFilenames() {
        listFilenames = new ArrayList<>(5);
    }

    protected BitmapsFilenames(Parcel in) {
        listFilenames = in.createStringArrayList();
    }

    public static final Creator<BitmapsFilenames> CREATOR = new Creator<BitmapsFilenames>() {
        @Override
        public BitmapsFilenames createFromParcel(Parcel in) {
            return new BitmapsFilenames(in);
        }

        @Override
        public BitmapsFilenames[] newArray(int size) {
            return new BitmapsFilenames[size];
        }
    };

    public List<String> getListFilenames() {
        return listFilenames;
    }

    public void setListFilenames(List<String> listFilenames) {
        this.listFilenames = listFilenames;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(listFilenames);
    }
}
